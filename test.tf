provider "google" {
  project     = var.project_id
  credentials = "${file("my-kubernetes-codelab-324804-e5088f20d4e2.json")}"
}

resource "google_container_cluster" "primary" {
  name     = "bhargav-12"
  location = "us-central1"
  remove_default_node_pool = true
  initial_node_count       = 1
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "my-node-pool"
  location   = "us-central1"
  cluster    = google_container_cluster.primary.name
  initial_node_count = 1

  node_config {
    machine_type = "e2-medium"
  }
}
